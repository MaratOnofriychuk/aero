import { HttpError } from '../../common/utils';
import FileModel from './file.model';

class FileRepository {
  create(data) {
    return FileModel.create(data);
  }

  delete(id, userId) {
    return FileModel.destroy({
      where: {
        id,
        userId,
      },
    });
  }

  update(id, userId, data) {
    return FileModel.update(data, {
      where: {
        id,
        userId,
      },
    });
  }

  findOneById(id, userId) {
    return FileModel.findOne({
      where: {
        id,
        userId,
      },
    });
  }

  async findOneByIdOrError(id, userId) {
    const file = await FileModel.findOne({
      where: {
        id,
        userId,
      },
    });

    if (!file) {
      throw new HttpError('File not found!', 404);
    }

    return file;
  }

  findAllForUser(userId, pagination) {
    return FileModel.findAndCountAll({
      offset: pagination.skip,
      limit: pagination.listSize,
      where: {
        userId,
      },
    });
  }
}

export default new FileRepository();
