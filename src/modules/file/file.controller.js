import { Router } from 'express';
import { authGuard } from '../../common/guards';
import { getPaginationParams, searchError } from '../../common/utils';
import fileService from './file.service';

export default (app) => {
  const router = Router();

  router.post('/upload', authGuard, searchError(async (req, res) => {
    const { user } = req;

    const fileEntity = await fileService.upload(req, user);

    res.status(201).json(fileEntity);
  }));

  router.get('/list', authGuard, searchError(async (req, res) => {
    const { user, query } = req;
    const pagination = getPaginationParams(query);

    const response = await fileService.getAll(user, pagination);

    res.status(200).json(response);
  }));

  router.delete('/delete/:id', authGuard, searchError(async (req, res) => {
    const { user, params: { id } } = req;

    await fileService.delete(id, user);

    res.status(200).send();
  }));

  router.get('/:id', authGuard, searchError(async (req, res) => {
    const { user, params: { id } } = req;

    const response = await fileService.getOne(id, user);

    res.status(200).json(response);
  }));

  router.get('/download/:id', authGuard, searchError(async (req, res) => {
    const { user, params: { id } } = req;

    await fileService.download(id, user, res);
  }));

  router.put('/update/:id', authGuard, searchError(async (req, res) => {
    const { user, params: { id } } = req;

    const updatedFile = await fileService.update(id, req, user);

    res.status(200).json(updatedFile);
  }));

  app.use('/file', router);
};
