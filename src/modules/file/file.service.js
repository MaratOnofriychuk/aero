import fsPromises from 'fs/promises';
import fs from 'fs';
import { getPaginationMeta, HttpError, uploadFileInStorage } from '../../common/utils';
import { PATH_STORAGE } from '../../config';
import fileRepository from './file.repository';

class FileService {
  async download(id, { id: userId }, res) {
    const { filepath } = await fileRepository.findOneByIdOrError(id, userId);

    const isFileExist = fs.existsSync(filepath);

    if (!isFileExist) {
      throw new HttpError('File not found!', 404)
    }

    fs.createReadStream(filepath).pipe(res);
  }

  async upload(httpStream, { id: userId }) {
    const fileData = await uploadFileInStorage(httpStream, PATH_STORAGE);

    const newFile = await fileRepository.create({
      ...fileData,
      userId,
    });

    return this.#buildResponseFile(newFile);
  }

  async update(id, httpStream, { id: userId }) {
    const oldFile = await fileRepository.findOneByIdOrError(id, userId);

    await fsPromises.unlink(oldFile.filepath);

    const newFileData = await uploadFileInStorage(httpStream, PATH_STORAGE);

    await fileRepository.update(id, userId, newFileData);

    const updatedFile = await fileRepository.findOneById(id, userId) 

    return this.#buildResponseFile(updatedFile);
  }

  async delete(id, { id: userId }) {
    const { filepath } = await fileRepository.findOneByIdOrError(id, userId);

    await fileRepository.delete(id, userId);
    await fsPromises.unlink(filepath);
  }

  async getOne(id, { id: userId }) {
    const file = await fileRepository.findOneByIdOrError(id, userId);

    return this.#buildResponseFile(file);
  }

  async getAll({ id: userId }, pagination) {
    const {
      count: total,
      rows,
    } = await fileRepository.findAllForUser(userId, pagination);

    const meta = getPaginationMeta({
      ...pagination,
      total,
    });

    return {
      rows: rows.map((r) => this.#buildResponseFile(r)),
      meta,
    };
  }

  #buildResponseFile(file) {
    delete file.dataValues.filepath;
    delete file.dataValues.userId;

    return file;
  }
}

export default new FileService();
