import fileController from './file/file.controller';
import userController from './user/user.controller';
import authController from './auth/auth.controller';

export default (app) => {
  fileController(app);
  userController(app);
  authController(app);
};
