import { HttpError } from '../../common/utils';
import UserModel from './user.model';

class UserRepository {
  create(data) {
    return UserModel.create(data);
  }

  update(id, data) {
    return UserModel.update(data, {
      where: {
        id,
      },
    });
  }

  async findOneOrError(where) {
    const user = await UserModel.findOne({ where });

    if (!user) {
      throw new HttpError('User not found!', 404);
    }

    return user;
  }

  findByIdOrError(id) {
    return this.findOneOrError({ id });
  }

  async findOneById(id) {
    return UserModel.findOne({
      where: {
        id,
      },
    });
  }
}

export default new UserRepository();
