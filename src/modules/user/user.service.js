import userRepository from './user.repository';

class UserService {
  async getInfoById(id) {
    const user = await userRepository.findByIdOrError(id);

    delete user.dataValues.password;
    delete user.dataValues.refreshToken;
    delete user.dataValues.tokens;

    return user;
  }
}

export default new UserService();
