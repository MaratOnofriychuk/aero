import { DataTypes } from 'sequelize';
import sequelize from '../../database';
import File from '../file/file.model';

const User = sequelize.define('User', {
  id: {
    type: DataTypes.STRING,
    allowNull: false,
    primaryKey: true,
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  refreshToken: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  tokens: {
    type: DataTypes.TEXT,
    allowNull: true,
  },
});

User.hasMany(File, {
  foreignKey: 'userId',
});

export default User;
