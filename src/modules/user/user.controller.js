import { Router } from 'express';
import { authGuard } from '../../common/guards';
import userService from './user.service';
import { searchError } from '../../common/utils';

export default (app) => {
  const router = Router();

  router.get('/info', authGuard, searchError(async (req, res) => {
    const { id } = req.user;

    const user = await userService.getInfoById(id);

    res.status(200).json(user);
  }));

  app.use('', router);
};
