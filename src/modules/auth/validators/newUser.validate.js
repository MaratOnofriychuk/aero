import validator from 'validator';
import { HttpError } from '../../../common/utils';

export default ({ password, login }) => {
  const isCorrectPassword = password.length > 6
    && password.length < 15;

  if (!isCorrectPassword) {
    throw new HttpError('Invalid password!', 400);
  }

  const isCorrectLogin = validator.isMobilePhone(login, 'ru-RU')
    || validator.isEmail(login);

  if (!isCorrectLogin) {
    throw new HttpError('Invalid login!', 400);
  }
};
