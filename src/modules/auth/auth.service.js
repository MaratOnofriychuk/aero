import {
  generateToken,
  hashPassword,
  isTokenExpired,
  isCorrectPassword,
  HttpError,
} from '../../common/utils';
import { JWT_CLIENT_KEY_EXPIRES_IN, JWT_REFRESH_TOKEN_EXPIRES_IN } from '../../config';
import userRepository from '../user/user.repository';

class AuthService {
  async signIn({ login, password }) {
    const userEntity = await userRepository.findByIdOrError(login);

    if (!isCorrectPassword(userEntity.password, password)) {
      throw new HttpError('Password invalid!', 400);
    }

    return this.#buildResponseUserWithTokens(userEntity);
  }

  async refresh(refreshToken) {
    if (isTokenExpired(refreshToken)) {
      throw new HttpError('Token invalid!', 400);
    }

    const userEntity = await userRepository.findOneOrError({ refreshToken });

    return this.#buildResponseUserWithTokens(userEntity);
  }

  async register({ login, password }) {
    const userEntity = await userRepository.findOneById(login);

    if (userEntity) {
      throw new HttpError('This login already exist!', 409);
    }

    const newUser = await userRepository.create({
      id: login,
      password: await hashPassword(password),
    });

    return this.#buildResponseUserWithTokens(newUser);
  }

  async logout(user, token) {
    const tokens = JSON.parse(user.tokens) || [];
    const filteredTokensString = JSON.stringify(tokens.filter((t) => t !== token));

    await userRepository.update(user.id, {
      tokens: filteredTokensString,
    });
  }

  async #buildResponseUserWithTokens(user) {
    const accessToken = generateToken(user, JWT_CLIENT_KEY_EXPIRES_IN);
    const refreshToken = generateToken(user, JWT_REFRESH_TOKEN_EXPIRES_IN);

    const tokens = JSON.parse(user?.tokens || '[]');
    const updatedTokensString = JSON.stringify(tokens.reduce(
      (p, token) => (isTokenExpired(token) ? p : p.concat(token)),
      [],
    ).concat(accessToken));

    await userRepository.update(user.id, {
      refreshToken,
      tokens: updatedTokensString,
    });

    delete user.dataValues.password;
    delete user.dataValues.refreshToken;
    delete user.dataValues.tokens;

    return {
      accessToken,
      refreshToken,
      user,
    };
  }
}

export default new AuthService();
