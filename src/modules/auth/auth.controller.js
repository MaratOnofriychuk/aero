import { Router } from 'express';
import { authGuard } from '../../common/guards';
import authService from './auth.service';
import { newUserValiation } from './validators';
import { searchError } from '../../common/utils';

export default (app) => {
  const router = Router();

  router.post('/signin', searchError(async ({ body }, res) => {
    const user = await authService.signIn(body);

    res.status(200).json(user);
  }));

  router.post('/signin/new_token', searchError(async (req, res) => {
    const { refreshToken } = req.body;

    const user = await authService.refresh(refreshToken);

    res.status(200).json(user);
  }));

  router.post('/signup', searchError(async ({ body }, res) => {
    newUserValiation(body);

    const newUser = await authService.register(body);

    res.status(201).json(newUser);
  }));

  router.get('/logout', authGuard, searchError(async (req, res) => {
    const { user, token } = req;

    await authService.logout(user, token);

    res.status(200).send();
  }));

  app.use('', router);
};
