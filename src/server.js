import express from 'express';
import cors from 'cors';
import modules from './modules';
import { PORT } from './config';

const app = express();

app.use(cors());
app.use(express.json());

modules(app);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
