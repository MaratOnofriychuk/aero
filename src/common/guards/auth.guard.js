import jwt from 'jsonwebtoken';
import { JWT_SECRET_KEY } from '../../config';
import userRepository from '../../modules/user/user.repository';

export default async (req, res, next) => {
  const token = req.headers['x-access-token'];
  let userJWT;

  try {
    userJWT = jwt.verify(token, JWT_SECRET_KEY);
  } catch {
    res.status(401).json({
      error: 'Invalid token!',
    });

    return;
  }

  if (!userJWT?.id) {
    res.status(401).json({
      error: 'Invalid token!',
    });

    return;
  }

  const user = await userRepository.findOneById(userJWT.id);

  if (!user) {
    res.status(401).json({
      error: 'User not found!',
    });

    return;
  }

  const isActiveToken = user.tokens.includes(token);

  if (!isActiveToken) {
    res.status(401).json({
      error: 'Invalid token!',
    });

    return;
  }

  req.token = token;
  req.user = user;

  next();
};
