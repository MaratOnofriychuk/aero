import jwt from 'jsonwebtoken';
import { JWT_SECRET_KEY } from '../../config';

export default (user, expiresIn) => jwt.sign({
  id: user.id,
}, JWT_SECRET_KEY, {
  expiresIn,
});
