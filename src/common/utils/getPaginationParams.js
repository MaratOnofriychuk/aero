export default (query) => {
  const page = prepareParameter(1, query.page);
  const listSize = prepareParameter(10, query.list_size);

  return {
    page,
    listSize,
    skip: (page - 1) * listSize,
  };
};

function prepareParameter(defaultValue, value) {
  let result = value ? Number(value || defaultValue) : defaultValue;
  if (result < 1) result = defaultValue;

  return result;
}
