import argon2 from 'argon2';

const { argon2d, hash } = argon2;

export default async (password) => hash(password, { type: argon2d });
