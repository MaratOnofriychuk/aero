export default (callback) => async (...args) => {
  const res = args[1];

  try {
    await callback(...args);
  } catch (e) {
    console.log(e);

    if (e.status) {
      res.status(e.status).json({
        error: e.message
      });
    } else {
      res.status(500).json({
        error: 'Server error!'
      });
    }
  }
};
