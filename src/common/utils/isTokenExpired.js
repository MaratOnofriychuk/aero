import jwt from 'jsonwebtoken';
import { JWT_SECRET_KEY } from '../../config';

export default (token) => {
  try {
    jwt.verify(token, JWT_SECRET_KEY);
    return false;
  } catch {
    return true;
  }
};
