import { verify } from 'argon2';

export default async (hashPassword, password) => verify(hashPassword, password);
