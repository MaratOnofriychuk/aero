export default ({ page, listSize, total }) => ({
  page,
  listSize,
  total,
  pages: Math.ceil(total / listSize),
});
