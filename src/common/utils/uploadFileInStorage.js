import formidable from 'formidable';
import fs from 'fs/promises';
import { v4 as createUUIDv4 } from 'uuid';

export default async (stream, baseDir) => {
  const randomPath = `${baseDir}/${createRandomPath()}`;

  await fs.mkdir(randomPath, { recursive: true });

  return saveFileInStorage(stream, randomPath);
};

function createRandomPath() {
  return createUUIDv4()
    .replace(/-/g, '')
    .slice(0, 8)
    .match(/(.{1,2})/gim)
    .join('/');
}

async function saveFileInStorage(stream, randomPath) {
  const form = formidable({
    uploadDir: randomPath,
    keepExtensions: true,
    filename: (...args) => args[0] + args[1],
  });

  return new Promise((resolve, reject) => {
    form.parse(stream, (error, _, files) => {
      if (error) {
        reject(error);
        return;
      }

      const {
        filepath,
        newFilename: name,
        mimetype: mimeType,
        size,
      } = files[Object.keys(files)[0]];

      const extension = filepath.split('.').at(-1);

      resolve({
        filepath,
        name,
        mimeType,
        size,
        extension,
      });
    });
  });
}
