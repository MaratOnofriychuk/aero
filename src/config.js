import path from 'path';
import { fileURLToPath } from 'url';

const filename = fileURLToPath(import.meta.url);
const dirname = path.dirname(filename);

export const PORT = process.env.PORT;
export const URL_DB = process.env.URL_DB;
export const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;
export const JWT_CLIENT_KEY_EXPIRES_IN = process.env.JWT_CLIENT_KEY_EXPIRES_IN;
export const JWT_REFRESH_TOKEN_EXPIRES_IN = process.env.JWT_REFRESH_TOKEN_EXPIRES_IN;
export const PATH_STORAGE = `${dirname}/${process.env.PATH_STORAGE}`;
export const MYSQL_DATABASE = process.env.MYSQL_DATABASE;
export const MYSQL_USER = process.env.MYSQL_USER;
export const MYSQL_PASSWORD = process.env.MYSQL_PASSWORD;
export const MYSQL_HOST = process.env.MYSQL_HOST;
