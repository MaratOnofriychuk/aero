module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: 'airbnb-base',
  overrides: [
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  rules: {
    'global-require': 0,
    'no-use-before-define': 0,
    'no-param-reassign': 0,
    'class-methods-use-this': 0,
  },
};
